package simple.cluster.entity

case class Counter(value: Long) {

	def inc(): Counter = Counter(value + 1)

	def <(other: Counter): Boolean = value < other.value

}

object Counter {

	def apply(): Counter = Counter(0)

}