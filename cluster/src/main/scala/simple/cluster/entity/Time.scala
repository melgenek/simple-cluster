package simple.cluster.entity

case class Time(value: Long) {

	def inc(): Time = Time(value + 1)

	def >(other: Time): Boolean = value > other.value

	def -(other: Time): Time = Time(value - other.value)

}

object Time {

	def apply(): Time = Time(0)

}
