package simple.cluster.actor

import akka.actor.{Actor, ActorLogging, ActorRef, Props}
import simple.cluster.actor.SampleBusinessActor.{LogCount, SendTick, Tick}
import simple.cluster.entity.{ClusterNodes, NodeUid}
import simple.cluster.messages.{GetClusterNodes, SetTickTimeout}
import simple.cluster.{BusinessRemoteActor, Node2ActorConverter}

import scala.concurrent.duration._

class SampleBusinessActor(val selfUid: NodeUid, val localNode: ActorRef)(implicit val node2Actor: Node2ActorConverter[BusinessRemoteActor])
	extends Actor with ActorLogging {

	import context._

	val LogTimeout: FiniteDuration = 1.second
	val PreviousPingWaitRatio = 3
	val TimeoutMessage = "timeout"
	var count = 0
	var timeout: FiniteDuration = 100.milliseconds

	override def preStart(): Unit = {
		system.scheduler.scheduleOnce(timeout, self, SendTick())
		system.scheduler.schedule(LogTimeout, LogTimeout, self, LogCount())
	}

	override def receive: Receive = {
		case Tick() => count += 1
		case LogCount() =>
			log.info(s"There were `$count` messages last second")
			count = 0
		case SetTickTimeout(newTimeout) =>
			timeout = newTimeout.milliseconds
			log.info(s"Timeout is set to `$newTimeout`")
		case SendTick() =>
			val extraActor = buildExtraActor()
			localNode.tell(GetClusterNodes(), extraActor)
			system.scheduler.scheduleOnce(PreviousPingWaitRatio * timeout, extraActor, TimeoutMessage)
			system.scheduler.scheduleOnce(timeout, self, SendTick())
	}

	//Effective Akka "Extra pattern"
	private def buildExtraActor(): ActorRef = {
		context.actorOf(Props(new Actor {
			override def receive: Receive = {
				case ClusterNodes(active, _) =>
					active.filter(_ != selfUid).map(_.node).foreach(node2Actor(_) ! Tick())
				case TimeoutMessage => context.stop(self)
			}
		}))
	}

}

object SampleBusinessActor {

	def props(selfUid: NodeUid, localNode: ActorRef)
					 (implicit node2Actor: Node2ActorConverter[BusinessRemoteActor]): Props = {
		Props(new SampleBusinessActor(selfUid, localNode))
	}

	/**
		* Broadcast message
		*/
	private case class Tick()

	/**
		* Periodical message notifying to send broadcast
		*/
	private case class SendTick()

	/**
		* Message notifying to log amount of received messages during last second
		*/
	private case class LogCount()

}
