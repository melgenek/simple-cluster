package simple.cluster.actor

import akka.actor.{Actor, ActorLogging, Props}
import simple.cluster._
import simple.cluster.entity._
import simple.cluster.messages._

import scala.collection.mutable
import scala.concurrent.duration._

/**
	* Actor representing p2p gossip heartbeat protocol.
	* Algorithm[1,2]:
	* {{{
	* 1. Each member maintains a list with each member's address and heartbeat counter `clusterState`.
	* At each time period Tgossip (`PingPeriod`) it increments its own heartbeat counter
	* and sends its list to one random member.
	* New gossip round is started upon receive of `simple.cluster.messages.SendPing` message
	*
	* 2. On receive of such gossip message `simple.cluster.messages.Ping`,
	* a member merges the list in the message with its own list
	* by maximum heartbeat counter for each member.
	*
	* 3. Each member also maintains for each other member in the list,
	* the last time (`simple.cluster.entity.Time`) that its corresponding heartbeat counter has increased.
	*
	* 4. If the heartbeat counter wasn't increased for time Tsuspend the member
	* is considered as failed. But failed members not removed from the list immediately,
	* they are put to suspended list `suspendedState`.
	* Tsuspend is passed upon receive of `simple.cluster.messages.TrySuspend` message
	*
	* 5. Member removed from membership lists after time period Tcleanup.
	* Tcleanup is passed upon receive of `simple.cluster.messages.TryCleanup` message
	*
	* }}}
	*
	* Optimization for choosing random node[3,4]: round robin, implemented with queue.
	*
	*
	* References:
	* 1. http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.20.5411&rep=rep1&type=pdf
	* 2. http://www.antonkharenko.com/2015/08/a-gossip-style-failure-detector.html
	* 3. http://www.cs.cornell.edu/~asdas/research/dsn02-swim.pdf
	* 4. https://prakhar.me/articles/swim/
	* 5. http://ringpop.readthedocs.io/en/latest/architecture_design.html#membership-protocol
	* 6. https://www.slideshare.net/jboner/the-road-to-akka-cluster-and-beyond
	*
	*/
class NodeActor private(val selfUid: NodeUid,
												val clusterState: mutable.Map[NodeUid, (Counter, Time)],
												var currentTime: Time,
												val seedNodes: List[Node]
											 )(implicit val node2Actor: Node2ActorConverter[NodeRemoteActor]) extends Actor with ActorLogging {

	import context._

	val CleanupToSuspendRatio = 2
	var pingQueue: mutable.Queue[Node] = mutable.Queue()
	fillPingQueue()

	val suspendedState: mutable.Map[NodeUid, (Counter, Time)] = mutable.Map()

	override def receive: Receive = {
		case Ping(state) =>
			log.debug(s"Ping message: $state")
			for ((node, newCounter) <- state) {
				updateNodeCounter(node, newCounter)
			}
			log.debug(s"Current cluster state: $clusterState")
		case SendPing() =>
			if (pingQueue.isEmpty) fillPingQueue()
			val nodeToPing: Node = pingQueue.dequeue()
			log.debug(s"Sending ping to $nodeToPing")
			log.debug(s"Current cluster state: $clusterState")
			updateSelfState()
			val stateToShare = clusterState.mapValues(_._1).toMap
			node2Actor(nodeToPing) ! Ping(stateToShare)
		case TrySuspend(timeout) =>
			for ((node, (counter, time)) <- clusterState) {
				if (currentTime - time > timeout) {
					clusterState -= node
					suspendedState += node -> (counter, time)
				}
			}
			val suspendPeriod = calcSuspendPeriod()
			system.scheduler.scheduleOnce(suspendPeriod * NodeActor.PingPeriod, self, TrySuspend(Time(suspendPeriod)))
		case TryCleanup(timeout) =>
			for ((node, (_, time)) <- suspendedState) {
				if (currentTime - time > timeout) {
					suspendedState -= node
				}
			}
			val cleanupPeriod = calcCleanupPeriod()
			system.scheduler.scheduleOnce(cleanupPeriod * NodeActor.PingPeriod, self, TryCleanup(Time(cleanupPeriod)))
		case GetClusterNodes() =>
			val activeNodes = clusterState.keys.toList
			val suspendedNodes = suspendedState.keys.toList
			sender() ! ClusterNodes(activeNodes, suspendedNodes)
		case KillNode() => System.exit(1)
	}

	private def calcSuspendPeriod() = pingQueue.size + clusterState.size // Tsuspend. Wait for a round of gossisping

	private def calcCleanupPeriod() = CleanupToSuspendRatio * calcSuspendPeriod() // Tsuspend. Wait for a round of gossisping

	/**
		* Updating heartbeat counter:
		* 1. If it's bigger than existing - put new counter and update time
		* 2. If suspended state contains node with lower heartbeat counter
		* consider node to be reconnected
		* 3. If suspended state contains node with higher heartbeat counter
		* then node is probably dead, do nothing
		* 4. Add new node if it hasn't been seen before
		*
		* @param node       Cluster node trying to update counter
		* @param newCounter Actual value of heartbeat counter
		*/
	private def updateNodeCounter(node: NodeUid, newCounter: Counter) {
		clusterState.get(node) match {
			case Some((prevCounter, _)) =>
				if (prevCounter < newCounter)
					clusterState += node -> (newCounter, currentTime)
			case None =>
				if (!suspendedState.contains(node))
					clusterState += node -> (newCounter, currentTime)
				else {
					val (suspendedCounter, _) = suspendedState(node)
					if (suspendedCounter < newCounter) {
						clusterState += node -> (newCounter, currentTime)
						suspendedState -= node
					}
				}
		}
	}

	/**
		* Update node's heartbeat counter before gossiping state
		*/
	private def updateSelfState() {
		currentTime = currentTime.inc()
		val (prevCounter, _): (Counter, Time) = clusterState(selfUid)
		clusterState += selfUid -> (prevCounter.inc(), currentTime)
	}

	/**
		* Add nodes from cluster to ping queue.
		* If there are no except node itself, then use seed nodes.
		*/
	private def fillPingQueue() = {
		pingQueue ++= clusterState.keys.filter(_ != selfUid).map(_.node)
		if (pingQueue.isEmpty) pingQueue ++= seedNodes.filter(_ != selfUid.node)
	}

}

object NodeActor {

	var PingPeriod: FiniteDuration = 1000.milliseconds
	var InitialDelay: FiniteDuration = 0.seconds

	def apply(selfUid: NodeUid, seedNodes: List[Node])
					 (implicit address2ActorConverter: Node2ActorConverter[NodeRemoteActor]): NodeActor = {
		val startTime = Time()
		val initialState = mutable.Map(selfUid -> (Counter(), startTime))
		new NodeActor(selfUid, initialState, startTime, seedNodes)
	}

	def props(selfUid: NodeUid, seedNodes: List[Node])
					 (implicit address2ActorConverter: Node2ActorConverter[NodeRemoteActor]): Props = {
		Props(NodeActor(selfUid, seedNodes))
	}

}