package simple.cluster.messages

import simple.cluster.entity.{Counter, NodeUid, Time}

/**
	* Cluster heartbeat
	* @param state Node uid with corresponding heartbeat counter
	*/
case class Ping(state: Map[NodeUid, Counter])

/**
	* Message that notifies cluster node to start new round of gossip
	*/
case class SendPing()

/**
	* Message that notifies cluster node to move unavailable nodes to suspended state
	*/
case class TrySuspend(timeout: Time)

/**
	* Message that notifies cluster node to remove suspended nodes
	*/
case class TryCleanup(timeout: Time)
