package simple.cluster

import akka.actor.{ActorSystem, ExtendedActorSystem}
import com.typesafe.config.ConfigFactory
import simple.cluster.Node2ActorConverters._
import simple.cluster.actor.{NodeActor, SampleBusinessActor}
import simple.cluster.entity._
import simple.cluster.messages.{SendPing, TryCleanup, TrySuspend}
import simple.cluster.util.ClusterUtil._

import scala.concurrent.ExecutionContext

object NodeMain {

	def main(args: Array[String]): Unit = {
		val config = ConfigFactory.load("node.conf")
		val system: ActorSystem = ActorSystem("cluster", config)
		implicit val context: ExecutionContext = system.dispatcher

		// Parse config
		val currentNode = getCurrentNode(system)
		val selfUid = NodeUid(currentNode)
		val seedNodes = parseSeedNodes(config.getString("akka.cluster.seed-nodes"))

		// Create actors
		val node = system.actorOf(NodeActor.props(selfUid, seedNodes), NodeRemoteActor.Id)
		system.actorOf(SampleBusinessActor.props(selfUid,node), BusinessRemoteActor.Id)

		// Schedule cluster events
		system.scheduler.schedule(NodeActor.InitialDelay, NodeActor.PingPeriod, node, SendPing())
		node ! TrySuspend(Time(Long.MaxValue))
		node ! TryCleanup(Time(Long.MaxValue))
	}

	def getCurrentNode(system: ActorSystem): Node = {
		val defaultAddress = system.asInstanceOf[ExtendedActorSystem].provider.getDefaultAddress
		Node(defaultAddress.host.get, defaultAddress.port.get.toString)
	}

}
