package simple.cluster.actor

import akka.actor.{Actor, ActorSelection, ActorSystem}
import akka.pattern.ask
import akka.testkit.TestActorRef
import akka.util.Timeout
import org.scalatest.{FlatSpec, Matchers}
import simple.cluster.{Node2ActorConverter, NodeRemoteActor}
import simple.cluster.entity._
import simple.cluster.messages._

import scala.concurrent.Await
import scala.concurrent.duration._

class NodeActorSpec extends FlatSpec with Matchers {

	implicit val actorSystem = ActorSystem()
	val nodeUid = NodeUid(Node("localhost", "9990"))
	implicit val testNode2ActorConverter = new Node2ActorConverter[NodeRemoteActor] {
		override def apply(node: Node)(implicit actorSystem: ActorSystem): ActorSelection = actorSystem.actorSelection(
			TestActorRef(new Actor {
				override def receive: Receive = {
					case _ =>
				}
			}).path
		)
	}

	"NodeActor" should "be have initialized with default values" in {
		val seedNodes = List(Node("host1", "port1"), Node("host2", "port2"))
		val actorRef = TestActorRef(NodeActor(nodeUid, seedNodes))

		actorRef.underlyingActor.selfUid should be(nodeUid)
		actorRef.underlyingActor.currentTime should be(Time())
		actorRef.underlyingActor.seedNodes should have length 2
	}

	"NodeActor" should "be have itself in cluster state" in {
		val actorRef = TestActorRef(NodeActor(nodeUid, List()))

		actorRef.underlyingActor.clusterState.head._1 should be(nodeUid)
	}

	"NodeActor" should "be have seed nodes in ping queue" in {
		val seedNodes = List(Node("host1", "port1"), Node("host2", "port2"))
		val actorRef = TestActorRef(NodeActor(nodeUid, seedNodes))

		actorRef.underlyingActor.pingQueue should have length 2
	}

	"NodeActor" should "not have itself in ping queue" in {
		val seedNodes = List(Node("host1", "port1"), Node("localhost", "9990"))
		val actorRef = TestActorRef(NodeActor(nodeUid, seedNodes))

		actorRef.underlyingActor.pingQueue should have length 1
	}

	"NodeActor" should "set node state when receive ping" in {
		val actorRef = TestActorRef(NodeActor(nodeUid, List()))
		actorRef ! Ping(Map(nodeUid -> Counter(1)))

		val (counter, localTime) = actorRef.underlyingActor.clusterState(nodeUid)
		counter should be(Counter(1))
		localTime should be(Time())
	}

	"NodeActor" should "update node state when receive ping" in {
		val actorRef = TestActorRef(NodeActor(nodeUid, List()))
		actorRef ! Ping(Map(nodeUid -> Counter(1)))
		actorRef ! Ping(Map(nodeUid -> Counter(2)))

		val (counter, localTime) = actorRef.underlyingActor.clusterState(nodeUid)
		counter should be(Counter(2))
	}

	"NodeActor" should "have at least seed nodes in ping queue" in {
		val seedNodes = List(Node("host1", "port1"), Node("host2", "port2"))
		val actorRef = TestActorRef(NodeActor(nodeUid, seedNodes))

		actorRef ! SendPing()
		actorRef.underlyingActor.pingQueue should have length 1

		actorRef ! SendPing()
		actorRef.underlyingActor.pingQueue should have length 0

		actorRef ! SendPing()
		actorRef.underlyingActor.pingQueue should have length 1
	}

	"NodeActor" should "update other nodes with its state" in {
		val tempActor = TestActorRef(new Actor {
			var receivedState: Map[NodeUid, Counter] = _

			override def receive: Receive = {
				case Ping(state) =>
					receivedState = state
			}
		})
		implicit val testNode2ActorConverter = new Node2ActorConverter[NodeRemoteActor] {
			override def apply(node: Node)(implicit actorSystem: ActorSystem): ActorSelection =
				actorSystem.actorSelection(tempActor.path)
		}
		val actorRef = TestActorRef(NodeActor(nodeUid, List(Node("host1", "port1"))))
		actorRef ! SendPing()

		val (uid, counter) = tempActor.underlyingActor.receivedState.head
		counter should be(Counter(1))
		uid should be(nodeUid)
	}

	"NodeActor" should "suspend node from cluster state when heartbeat timeout" in {
		val seedNode = Node("host1", "port1")
		val actorRef = TestActorRef(NodeActor(nodeUid, List(seedNode)))
		actorRef ! Ping(Map(NodeUid(seedNode) -> Counter()))
		actorRef ! SendPing()

		actorRef ! TrySuspend(Time(0))
		actorRef.underlyingActor.clusterState should have size 1
		actorRef.underlyingActor.suspendedState.head._1.node should be(seedNode)
	}

	"NodeActor" should "not add node to cluster if suspended with same state" in {
		val seedNode = Node("host1", "port1")
		val seedNodeUid = NodeUid(seedNode)
		val actorRef = TestActorRef(NodeActor(nodeUid, List(seedNode)))
		actorRef ! Ping(Map(seedNodeUid -> Counter()))
		actorRef ! SendPing()
		actorRef ! TrySuspend(Time(0))

		actorRef ! Ping(Map(seedNodeUid -> Counter()))
		actorRef.underlyingActor.clusterState should have size 1
		actorRef.underlyingActor.suspendedState.head._1.node should be(seedNode)
	}

	"NodeActor" should "add node to cluster if suspended with older state" in {
		val seedNode = Node("host1", "port1")
		val seedNodeUid = NodeUid(seedNode)
		val actorRef = TestActorRef(NodeActor(nodeUid, List(seedNode)))
		actorRef ! Ping(Map(seedNodeUid -> Counter()))
		actorRef ! SendPing()
		actorRef ! TrySuspend(Time(0))

		actorRef ! Ping(Map(seedNodeUid -> Counter(1)))
		actorRef.underlyingActor.clusterState should have size 2
		actorRef.underlyingActor.suspendedState should have size 0
	}

	"NodeActor" should "cleanup suspended node" in {
		val seedNode = Node("host1", "port1")
		val actorRef = TestActorRef(NodeActor(nodeUid, List(seedNode)))
		actorRef ! Ping(Map(NodeUid(seedNode) -> Counter()))
		actorRef ! SendPing()
		actorRef ! TrySuspend(Time(0))

		actorRef ! TryCleanup(Time(0))
		actorRef.underlyingActor.clusterState should have size 1
		actorRef.underlyingActor.suspendedState should have size 0
	}

	it should "get cluster nodes from NodeActor" in {
		val seedNode = Node("host1", "port1")
		val actorRef = TestActorRef(NodeActor(nodeUid, List(seedNode)))
		actorRef ! Ping(Map(NodeUid(seedNode) -> Counter()))
		actorRef ! SendPing()
		actorRef ! TrySuspend(Time(0))
		implicit val timeout = Timeout(1.second)

		val future = actorRef ? GetClusterNodes()
		val clusterNodes = Await.result(future.mapTo[ClusterNodes], 1.second)

		clusterNodes.active should have length 1
		clusterNodes.suspended should have length 1
	}

}
