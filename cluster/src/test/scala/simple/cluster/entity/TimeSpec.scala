package simple.cluster.entity

import org.scalatest.{FlatSpec, Matchers}

class TimeSpec extends FlatSpec with Matchers {

	"Incremented Time" should "be more than previous time" in {
		val time1 = Time()
		val time2 = time1.inc()

		time2 > time1 should be(true)
	}



}
