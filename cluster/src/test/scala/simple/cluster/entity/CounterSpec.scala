package simple.cluster.entity

import org.scalatest.{FlatSpec, Matchers}

class CounterSpec extends FlatSpec with Matchers {

	"Counter" should "be less than incremented counter" in {
		val counter1 = Counter()
		val counter2 = counter1.inc()

		counter1 < counter2 should be(true)
	}

}
