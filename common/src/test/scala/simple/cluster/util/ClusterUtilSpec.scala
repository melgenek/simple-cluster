package simple.cluster.util

import org.scalatest.{FlatSpec, Matchers}

import simple.cluster.entity.Node

class ClusterUtilSpec extends FlatSpec with Matchers {

	"ServerMain" should "be have initialized with default values" in {
		val seedNodes = ClusterUtil.parseSeedNodes("localhost:9090;127.0.0.1:9091")

		seedNodes.head should be(Node("localhost", "9090"))
		seedNodes.tail.head should be(Node("127.0.0.1", "9091"))
	}

}
