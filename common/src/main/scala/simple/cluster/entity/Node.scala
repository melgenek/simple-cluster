package simple.cluster.entity

import java.util.UUID

/**
	* Logical representation of remote cluster node
	*/
case class Node(host: String, port: String)

/**
	* Unique id for cluster node
	* @param node Remote address
	* @param uid additional UUID that shows if the node was failed and restarted
	*            or recovered with previous state from network issues/high load etc.
	*/
case class NodeUid(node: Node, uid: UUID = UUID.randomUUID())

case class ClusterNodes(active: List[NodeUid] = List(), suspended: List[NodeUid] = List())

