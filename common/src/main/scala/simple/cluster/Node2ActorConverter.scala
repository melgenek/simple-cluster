package simple.cluster

import akka.actor.{ActorSelection, ActorSystem}
import simple.cluster.entity.Node

sealed trait RemoteActor

trait NodeRemoteActor extends RemoteActor

object NodeRemoteActor {
	val Id = "node"
}

trait BusinessRemoteActor extends RemoteActor

object BusinessRemoteActor {
	val Id = "business"
}

/**
	* Converts logical [[simple.cluster.entity.Node]] to akka actor selection for message sending.
	* Mock implementations can be used in tests.
	*
	* @tparam T Specifies kind of remote actor, i.e. [[simple.cluster.NodeRemoteActor]]
	*/
trait Node2ActorConverter[T <: RemoteActor] {
	def apply(node: Node)(implicit actorSystem: ActorSystem): ActorSelection
}

object Node2ActorConverters {

	implicit val clusterNode2Actor: Node2ActorConverter[NodeRemoteActor] = new Node2ActorConverter[NodeRemoteActor] {

		override def apply(node: Node)(implicit actorSystem: ActorSystem): ActorSelection = {
			actorSystem.actorSelection(s"akka.tcp://cluster@${node.host}:${node.port}/user/${NodeRemoteActor.Id}")
		}

	}

	implicit val business2Actor: Node2ActorConverter[BusinessRemoteActor] = new Node2ActorConverter[BusinessRemoteActor] {

		override def apply(node: Node)(implicit actorSystem: ActorSystem): ActorSelection = {
			actorSystem.actorSelection(s"akka.tcp://cluster@${node.host}:${node.port}/user/${BusinessRemoteActor.Id}")
		}

	}


}