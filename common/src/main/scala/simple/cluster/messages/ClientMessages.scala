package simple.cluster.messages

case class GetClusterNodes()

case class KillNode()

case class SetTickTimeout(timeout: Int)
