package simple.cluster.util

import simple.cluster.entity.Node

object ClusterUtil {

	def parseSeedNodes(str: String): List[Node] = {
		str.split(";").map(arg => {
			val hostPort = arg.split(":")
			Node(hostPort(0), hostPort(1))
		}).toList
	}

}
