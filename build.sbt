name := "simple-cluster"
version := "1.0"
resolvers += Resolver.bintrayRepo("hseeberger", "maven")

lazy val versions = new {
	val akka = "2.5.2"
	val scalaTest = "3.0.3"
	val akkaSse = "3.0.0"
}

lazy val dockerPackageSettings = Seq(
	test in assembly := {},
	sbt.Keys.`package` := {
		assembly.value
		docker.value
		file("")
	},
	dockerfile in docker := {
		val artifact: File = assembly.value
		val artifactTargetPath = s"/app/${artifact.name}"
		new Dockerfile {
			from("java")
			add(artifact, artifactTargetPath)
			entryPoint("java", "-jar", artifactTargetPath)
			expose(9990)
		}
	},
	imageNames in docker := Seq(
		ImageName(s"simple-cluster/${name.value}:latest")
	),
	assembly := {
		(compile in Compile).value
		assembly.value
	}
	//,
	//	assemblyMergeStrategy in assembly := {
	//		case PathList("org", "newsclub", _*) => MergeStrategy.first
	//		case PathList("org", "apache", _*) => MergeStrategy.first
	//		case x if x.endsWith("io.netty.versions.properties") => MergeStrategy.discard
	//		case other => MergeStrategy.defaultMergeStrategy(other)
	//	}
)

lazy val scalaVersionSettings = Seq(
	scalaVersion := "2.11.11"
)

lazy val common = project.in(file("common"))
	.settings(scalaVersionSettings)
	.settings(libraryDependencies ++= Seq(
		"com.typesafe.akka" %% "akka-actor" % versions.akka,
		"com.typesafe.akka" %% "akka-remote" % versions.akka,
		"com.typesafe.akka" %% "akka-testkit" % versions.akka % "test",
		"org.scalatest" %% "scalatest" % versions.scalaTest % "test"
	))

lazy val cluster = project.in(file("cluster"))
	.settings(scalaVersionSettings)
	.settings(dockerPackageSettings)
	.dependsOn(common % "compile->compile;test->test")
	.enablePlugins(DockerPlugin)

lazy val client = project.in(file("client"))
	.settings(scalaVersionSettings)
	.dependsOn(common % "compile->compile;test->test")

lazy val admin = project.in(file("admin"))
	.settings(scalaVersionSettings)
	.settings(dockerPackageSettings)
	.settings(libraryDependencies ++= Seq(
		"de.heikoseeberger" %% "akka-sse" % versions.akkaSse,
		"com.typesafe.akka" %% "akka-http-spray-json" % "10.0.7",
		"com.spotify" % "docker-client" % "8.7.2"
		//, "com.github.docker-java" % "docker-java" % "3.0.10"
	))
	.dependsOn(client % "compile->compile;test->test")
	.enablePlugins(DockerPlugin)

lazy val root = project.in(file("."))
	.settings(scalaVersionSettings)
	.aggregate(common, cluster, client, admin)
