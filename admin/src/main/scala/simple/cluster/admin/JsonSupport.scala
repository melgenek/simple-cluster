package simple.cluster.admin

import java.util.UUID

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import simple.cluster.entity.{ClusterNodes, Node, NodeUid}
import simple.cluster.messages.SetTickTimeout
import spray.json.{DefaultJsonProtocol, JsString, JsValue, RootJsonFormat}

trait JsonSupport extends SprayJsonSupport with DefaultJsonProtocol {

	implicit object UuidJsonFormat extends RootJsonFormat[UUID] {
		def write(x: UUID) = JsString(x.toString)

		def read(value: JsValue): UUID = value match {
			case JsString(x) => UUID.fromString(x)
			case x => throw new IllegalArgumentException("Expected UUID as JsString, but got " + x)
		}
	}

	implicit val setTimeout: RootJsonFormat[SetTickTimeout] = jsonFormat1(SetTickTimeout)
	implicit val nodeFormat: RootJsonFormat[Node] = jsonFormat2(Node)
	implicit val nodeUidFormat: RootJsonFormat[NodeUid] = jsonFormat2(NodeUid)
	implicit val clusterNodesFormat: RootJsonFormat[ClusterNodes] = jsonFormat2(ClusterNodes)

}
