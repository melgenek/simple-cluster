package simple.cluster.admin

import com.spotify.docker.client.DefaultDockerClient
import com.spotify.docker.client.messages.{ContainerConfig, HostConfig}

class NodeRunner {

	val docker: DefaultDockerClient = DefaultDockerClient.fromEnv().build()

	def runNode() {
		val hostConfig = HostConfig.builder().networkMode("simple-cluster-network").build()
		val containerConfig = ContainerConfig.builder()
			.hostConfig(hostConfig)
			.image("simple-cluster/cluster")
			.env(s"NODE_IP=", "NODE_PORT=0", "SEED_NODES=seed1:9990;seed2:9991")
			.build()
		val container = docker.createContainer(containerConfig)
		docker.startContainer(container.id())
	}

}
