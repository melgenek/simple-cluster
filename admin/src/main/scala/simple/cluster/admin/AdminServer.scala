package simple.cluster.admin


import akka.NotUsed
import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Directives
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.Source
import com.typesafe.config.{Config, ConfigFactory}
import de.heikoseeberger.akkasse.scaladsl.marshalling.EventStreamMarshalling
import de.heikoseeberger.akkasse.scaladsl.model.ServerSentEvent
import simple.cluster.Node2ActorConverters
import simple.cluster.client.ClusterClient
import simple.cluster.entity.Node
import simple.cluster.messages.SetTickTimeout
import simple.cluster.util.ClusterUtil._

import scala.concurrent.ExecutionContext
import scala.concurrent.duration.DurationInt

object AdminServer extends JsonSupport {

	val config: Config = ConfigFactory.load("admin.conf")
	val seedNodes: List[Node] = parseSeedNodes(config.getString("akka.cluster.seed-nodes"))

	implicit val system = ActorSystem("admin", config)
	implicit val context: ExecutionContext = system.dispatcher
	implicit val mat = ActorMaterializer()

	val nodeRunner = new NodeRunner()

	def main(args: Array[String]): Unit = {
		Http().bindAndHandle(route, config.getString("admin.host"), config.getInt("admin.port"))
	}

	private def route = {
		import Directives._
		import EventStreamMarshalling._
		import Node2ActorConverters._
		import spray.json._

		def assets =
			pathSingleSlash(
				getFromResource("web/index.html")
			)

		def clusterState =
			path("cluster" / "state") {
				get {
					complete {
						var clusterClient = ClusterClient(seedNodes, 1)
						Source
							.tick(0.seconds, 2.seconds, NotUsed)
							.mapAsync(1)(_ => clusterClient.getClusterNodes())
							.map { case (nodes, newClient) =>
								clusterClient = newClient
								nodes.toJson.toString
							}
							.map(ServerSentEvent(_))
					}
				}
			}

		def killNode =
			path("nodes") {
				delete {
					entity(as[Node]) { node =>
						var clusterClient = ClusterClient(seedNodes)
						clusterClient.killNode(node)
						complete(StatusCodes.Accepted)
					}
				}
			}

		def tickTimeout =
			path("tickTimeout") {
				post {
					entity(as[SetTickTimeout]) { timeout =>
						var clusterClient = ClusterClient(seedNodes, 1)
						onComplete(clusterClient.setTickTimeout(timeout))(_ => complete(StatusCodes.Accepted))
					}
				}
			}

		def runNode =
			path("runNode") {
				post {
					nodeRunner.runNode()
					complete(StatusCodes.Accepted)
				}
			}

		assets ~
			clusterState ~
			killNode ~
			tickTimeout ~
			runNode
	}

}

