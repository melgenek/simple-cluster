package simple.cluster.client

import akka.actor.ActorSystem
import akka.pattern.ask
import akka.util.Timeout
import simple.cluster.client.ClusterClient.ClusterResult
import simple.cluster.entity.{ClusterNodes, Node}
import simple.cluster.messages.{GetClusterNodes, KillNode, SetTickTimeout}
import simple.cluster.{BusinessRemoteActor, Node2ActorConverter, NodeRemoteActor}

import scala.concurrent.duration._
import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Random, Success}

case class ClusterClient private(seedNodes: List[Node], clientNodes: List[Node], retry: Int)(
	implicit val system: ActorSystem, val timeout: Timeout, val context: ExecutionContext) {

	require(seedNodes.nonEmpty)
	require(clientNodes.nonEmpty)

	/**
		* Get nodes in cluster.
		* @param attempts Count of retry attempts
		* @param node2Actor resolver to convert logical Node to remote actor
		* @return
		*/
	def getClusterNodes(attempts: Int = retry)(implicit node2Actor: Node2ActorConverter[NodeRemoteActor]): Future[ClusterResult[ClusterNodes]] = {
		val node = clientNodes(Random.nextInt(clientNodes.size))
		val future = node2Actor(node) ? GetClusterNodes()
		val result = future.mapTo[ClusterNodes].map(nodes => {
			val newClient = this.copy(clientNodes = nodes.active.map(_.node))
			(nodes, newClient)
		})
		if (attempts > 0) result.recoverWith {
			case _ => createNewClientAfterFail(node).getClusterNodes(attempts = attempts - 1)
		}
		else result.recover {
			case _ =>
				val emptyClusterNodes = ClusterNodes()
				val newClient: ClusterClient = createNewClientAfterFail(node)
				(emptyClusterNodes, newClient)
		}
	}

	private def createNewClientAfterFail(lastTriedNode: Node) = {
		val newClientNodes = clientNodes.filter(_ != lastTriedNode)
		val newClient = if (newClientNodes.nonEmpty) this.copy(clientNodes = newClientNodes)
		else this.copy(clientNodes = seedNodes)
		newClient
	}

	/**
		* Send kill request to node.
		* Node won't stop if it's unreachable from client
		* @param node Logical remote Node
		* @param node2Actor resolver to convert logical Node to remote actor
		*/
	def killNode(node: Node)(implicit node2Actor: Node2ActorConverter[NodeRemoteActor]): Unit = {
		node2Actor(node) ! KillNode()
	}

	/**
		* Send new broadcast timeout to all nodes
		* @param setTimeout Message with actual value of timeout
		* @param clusterNode2Actor resolver to convert logical cluster Node to remote actor
		* @param node2Actor resolver to convert logical business Node to remote actor
		* @return
		*/
	def setTickTimeout(setTimeout: SetTickTimeout)(
		implicit clusterNode2Actor: Node2ActorConverter[NodeRemoteActor],
		node2Actor: Node2ActorConverter[BusinessRemoteActor]): Future[Unit] = {
		getClusterNodes().andThen {
			case Success((nodes, _)) =>
				nodes.active.map(_.node).foreach(node2Actor(_) ! setTimeout)
			case _ =>
		} map (_ => ())
	}

}

object ClusterClient {

	def apply(seedNodes: List[Node], retry: Int = 0)(
		implicit system: ActorSystem = ActorSystem("client"),
		timeout: Timeout = Timeout(2.seconds),
		context: ExecutionContext = scala.concurrent.ExecutionContext.Implicits.global
	): ClusterClient = ClusterClient(seedNodes, seedNodes, retry)

	type ClusterResult[T] = (T, ClusterClient)

}
