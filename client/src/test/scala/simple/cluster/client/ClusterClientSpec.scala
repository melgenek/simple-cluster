package simple.cluster.client

import akka.actor.{Actor, ActorSelection, ActorSystem}
import akka.testkit.TestActorRef
import akka.util.Timeout
import org.scalatest.{AsyncFlatSpec, Matchers}
import simple.cluster.entity._
import simple.cluster.messages.{GetClusterNodes, KillNode}
import simple.cluster.{Node2ActorConverter, NodeRemoteActor}

import scala.concurrent.duration._

class ClusterClientSpec extends AsyncFlatSpec with Matchers {

	implicit val actorSystem = ActorSystem()
	implicit val timeout: Timeout = Timeout(1.millisecond)

	val activeNodes = List(NodeUid(Node("host", "port")))

	"ClusterClient" should "set active nodes to client list" in {
		val clusterClient = ClusterClient(List(Node("seed", "seedPort")))
		implicit val testNode2ActorConverter = new Node2ActorConverter[NodeRemoteActor] {
			override def apply(node: Node)(implicit actorSystem: ActorSystem): ActorSelection = actorSystem.actorSelection(
				TestActorRef(new Actor {
					override def receive: Receive = {
						case GetClusterNodes() => sender() ! ClusterNodes(activeNodes, List())
					}
				}).path
			)
		}

		clusterClient.getClusterNodes().map { case (nodes, client) =>
			client.clientNodes should be(List(Node("host", "port")))
		}
	}

	"ClusterClient" should "set seed nodes to client list when no answer" in {
		val clusterClient = ClusterClient(List(Node("seed1", "seedPort1"), Node("seed2", "seedPort2"), Node("seed3", "seedPort3")))
		implicit val failingNode2ActorConverter = new Node2ActorConverter[NodeRemoteActor] {
			override def apply(node: Node)(implicit actorSystem: ActorSystem): ActorSelection = actorSystem.actorSelection(
				TestActorRef(new Actor {
					override def receive: Receive = {
						case _ =>
					}
				}).path
			)
		}

		clusterClient.getClusterNodes().map { case (nodes, client) =>
			client.clientNodes should have size 2
		}
	}

	"ClusterClient" should "set seed nodes after multiple retry when no answer" in {
		val clusterClient = ClusterClient(List(Node("seed1", "seedPort1"), Node("seed2", "seedPort2"), Node("seed3", "seedPort3")), 2)
		implicit val failingNode2ActorConverter = new Node2ActorConverter[NodeRemoteActor] {
			override def apply(node: Node)(implicit actorSystem: ActorSystem): ActorSelection = actorSystem.actorSelection(
				TestActorRef(new Actor {
					override def receive: Receive = {
						case _ =>
					}
				}).path
			)
		}

		clusterClient.getClusterNodes().map { case (nodes, client) =>
			client.clientNodes should have size 3
		}
	}

	"ClusterClient" should "send KillNode() message" in {
		val clusterClient = ClusterClient(List(Node("seed1", "seedPort1"), Node("seed2", "seedPort2"), Node("seed3", "seedPort3")))
		val testActorRef = TestActorRef(new Actor {
			var killed = false

			override def receive: Receive = {
				case KillNode() => killed = true
			}
		})
		implicit val killNode2ActorConverter = new Node2ActorConverter[NodeRemoteActor] {
			override def apply(node: Node)(implicit actorSystem: ActorSystem): ActorSelection = actorSystem.actorSelection(testActorRef.path
			)
		}

		clusterClient.killNode(Node("any", "any"))
		testActorRef.underlyingActor.killed should be(true)
	}


}