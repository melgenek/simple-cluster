### Running the app

Requirements:
* sbt
* docker (for Windows mark `expose daemon on tcp://localhost:2375` in Settings)

Steps (untested on Mac):

1) execute build

```
sudo sbt package
```

This doesn't include tests. They can be run using ```sbt test```

2) Create docker network
 
```
sudo docker network create simple-cluster-network
```

3) From the project directory run

```
sudo docker-compose up
```
This commands starts 2 seed nodes, sample node and web app.

Web app is accessible on `localhost:8000`

---
### Admin interface description

* left side called `Active nodes` shows cluster nodes
* right side `Suspended nodes` refers to clustering algorithm 
and shows nodes that are most likely dead
* state is updated once in 2 seconds via sse events asking different
nodes in cluster so cluster state can be change on UI each time
* clicking `Stop` button will try to kill node without guarantee for result.
If the node is not accessible from admin server then it stays alive.
It can take up about `20 seconds` to find down nodes in cluster of `7` nodes 
(more detail in [Clustering algorithm](#clustering-algorithm))
* Run node starts additional docker container of `simple-cluster/cluster` image

---
### Clustering algorithm

The algorithm is p2p gossip heartbeat algorithm. 
Implementation and description can be found in `NodeActor`.
As it's gossip algorithm each period of time (`1 second`) 
it send a message to another node in cluster containing known cluster state.
In total cluster nodes send `n` messages each containing `map[node id, node heartbeat counter]` 
proportional in size to `n`, where `n` is number of nodes.

Image illustrating the algorithm can be found in root of project `connect_to_cluster.png`

Pros of algorithm:
* no single point of failure. 
If one node fails, others continue to work.

Cons of algorithm:
* gossip payload is proportional to size of cluster.
If the size of one entry in map is `1kb` then cluster 
of `1000` nodes sends `1mb(each node)*1000(node count)=1gb` per second.

* takes time to find dead nodes 
because each node marks nodes as dead by itself.
Can take up to `2*n(number of nodes)*1s(gossip period)` to suspend node
and twice as much to remove it totally.

* algorithm doesn't show pairwise coneections failures between nodes, 
it only shows totally unreachable nodes. 
This can result in unreachable nodes while broadcasting. 

---
### Logging broadcast

According to task a broadcast to neighbour nodes is implemented
in `SampleBusinessActor`. 
Each broadcast round is the following:
* get cluster nodes from `NodeActor`
* send message to all nodes in cluster

Results:
* theoretically each node gets `n(number of nodes)` once in period of time.
So if `n=5` and `timeout=100ms` than there should be `5* (1s/100ms) = 50m/s` messages per second.
Total number of messages in cluster is `n^2`
* in practice roundtrip to get cluster nodes takes time so 
amount of message is less than theoretical.
* making `timeout` bigger, for example from `100ms` to `1s` makes 
the received messages count closer to theoretical limit.
* making `timeout` lower than `100ms` doesn't give bonus load
because of additional call to get cluster state.
For local env it is because of akka processing,
for remote it should be because of network latency.

---
### General observations

* Local testing can be inaccurate because of threading.
Akka uses Netty timer to schedule actor processing 
based on small amount of threads. 
Multiple local instances start more threads so that
context switching comes into place.
